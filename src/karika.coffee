# Description:
#   Random bir karikatur gonderir
#
# Dependencies:
#   None
#
# Configuration:
#   None
#
# Commands:
#   güldür - Bir karikatur goruntule
#
# Author:
#   Cagri Ersen <cagri.ersen@gmail.com>

images = require './data/images.json'

module.exports = (robot) ->
  robot.respond /güldür\b/i, (msg) ->
    msg.send msg.random images