# hubot-karika

Hubot için rastgele karikatur URL'i üretici.

## Kurulum

NPM paketini kurun: `npm install hubot-karika` ve 
`external-scripts.json` dosyasına **hubot-karika** satırını ekleyin:

```json
[
  "hubot-karika"
]
```

## Kullanım şekli

```
bot-ismi güldür
```

**Not** : Karikatürler, karikaturella.wordpress.com ve 
karikaturistan.wordpress.com adreslerinden gelmektedir.